# News_filter

## Description

A django application that uses a third-party API from newsapi to search for recent news articles by search word, or access them by search category.

## How to start this project locally:

**Starting a project from git clone:**

**Set up environment:**

1. Fork and clone new git repository:

   1. login to Gitlab, fork project to my git, copy clone url

   ```powershell
   git clone url...
   ```

2. Create and activate virtual environment:

   ```powershell
   python -m venv .venv
   .\.venv\Scripts\Activate.ps1
   ```

3. Install and update pip, and install new pip requirements

   ```powershell
   python -m pip install --upgrade pip
   pip install -r requirements.txt
   ```

**Configure Django application:**

If you delete the database you will need to redo these steps:

1. Run the migrations

   ```powershell
   python manage.py migrate
   ```

2. Create a super user

   ```powershell
   python manage.py createsuperuser
   [enter name, email and create password]
   ```

3. Test your application:

   ```powershell
   python manage.py runserver
   ```

   Open [http://localhost:8000](http://localhost:8000) to see if it’s working! (HTTP and not HTTPS!)

4. If you need to exit Django / stop it running in the terminal:

   ```powershell
   Ctrl C
   ```

## How to restart this project later:

1. Activate the virtual environment in the terminal:

   ```powershell
   .\.venv\Scripts\Activate.ps1
   ```

2. Run the server in the terminal:

   ```powershell
   python manage.py runserver
   ```

3. See the project at localhost:8000

## Images

![Search bar for top news](/images/top_news_search_bar.png "Search bar")
![Search bar example](/images/game_dev_news_search.png "Game dev news")
![News categories](/images/categories.png "Categories")
![News category example](/images/tech_category_top_news.png "Tech news")
![How to edit the search categories](/images/edit_topics.png "Edit topics")

from django.shortcuts import render
from django.urls import reverse_lazy
from newsapi import NewsApiClient
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import NewsTopic, SearchBar
import environ

env = environ.Env()
environ.Env.read_env()

# Create your views here.
def index(request, query_input):

  KEY = env('API_KEY')

  newsapi = NewsApiClient(api_key=KEY)

  # if query_input in const.categories:
  topnews = newsapi.get_top_headlines(category=query_input)
  # topnews = newsapi.get_everything(qintitle=query_input)
  # print(topnews)
  # print(query_input)

  latest = topnews["articles"]
  news = []
  descr = []
  image = []
  link = []

  for i in range(3):
    # limited to 3 articles for simplicity with css work
    # for i in range(len(latest)): would include all articles
    info = latest[i]
    news.append(info["title"])
    descr.append(info["description"])
    image.append(info["urlToImage"])
    link.append(info["url"])
    # print("$$$$$$$", info)

  # print(latest[0])
  # prints the first item from the topnews articles list

  my_list = list(zip(news, descr, image, link))

    # if we remove list from this zip, we can iterate
    # through the 3 items we added above at a time
    # if we add list(), our list will have 3 items
    # so we can use index 0 to 2 in our html template

  context = {
    "my_list": my_list,
    "query_input": query_input,
  }

  return render(request, "index.html", context)

class NewsTopicList(ListView):
  model = NewsTopic
  template_name = "intro.html"

class NewsTopicSelect(ListView):
  model = NewsTopic
  template_name = "select.html"

class NewsTopicCreate(CreateView):
  model = NewsTopic
  template_name = "new.html"
  fields = ["topic_name"]

  def get_success_url(self):
    return reverse_lazy("intro")

class NewsTopicUpdate(UpdateView):
  model = NewsTopic
  template_name = "edit.html"
  fields = ["topic_name"]

  def get_success_url(self):
    return reverse_lazy("topics")

class NewsTopicDelete(DeleteView):
  model = NewsTopic
  template_name = "delete.html"

  def get_success_url(self):
    return reverse_lazy("topics")


# creating a view to search by name in title:
def search(request, search_bar):

  KEY = env('API_KEY')

  newsapi = NewsApiClient(api_key=KEY)
  everything = newsapi.get_everything(q=search_bar)

  latest = everything["articles"]
  news = []
  descr = []
  image = []
  link = []

  for i in range(3):
    info = latest[i]
    news.append(info["title"])
    descr.append(info["description"])
    image.append(info["urlToImage"])
    link.append(info["url"])

  # print(latest[0])

  my_list = list(zip(news, descr, image, link))

  context = {
    "my_list": my_list,
    "search_bar": search_bar,
  }

  return render(request, "search-detail.html", context)

class SearchBarCreate(CreateView):
  model = SearchBar
  template_name = "search.html"
  fields = ["search"]

  def get_success_url(self):
    return reverse_lazy("search-detail", args=[self.object] )

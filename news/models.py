from django.db import models
# from django import forms

# Create your models here.
class NewsTopic(models.Model):
  topic_name = models.CharField(max_length=100)

  def __str__(self):
    return self.topic_name

class SearchBar(models.Model):
  search = models.CharField(max_length=100)

  def __str__(self):
    return self.search

from django.urls import path
from .views import index, search, NewsTopicList, NewsTopicCreate, NewsTopicUpdate, NewsTopicSelect, NewsTopicDelete, SearchBarCreate

urlpatterns = [
  path("index/<str:query_input>", index, name="index"),
  path("", NewsTopicList.as_view(), name="intro"),
  path("new/", NewsTopicCreate.as_view(), name="new"),
  path("edit/<int:pk>/", NewsTopicUpdate.as_view(),name="edit"),
  path("topics/", NewsTopicSelect.as_view(), name="topics"),
  path("delete/<int:pk>", NewsTopicDelete.as_view(), name="delete"),
  path("search/<str:search_bar>", search, name="search-detail"),
  path("search/", SearchBarCreate.as_view(), name="search")
]

from django.contrib import admin
from .models import NewsTopic, SearchBar

# Register your models here.
admin.site.register(NewsTopic)
admin.site.register(SearchBar)
